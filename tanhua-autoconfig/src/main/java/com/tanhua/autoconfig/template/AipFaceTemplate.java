package com.tanhua.autoconfig.template;

import com.baidu.aip.face.AipFace;
import com.tanhua.autoconfig.properties.AipFaceProperties;
import com.tanhua.autoconfig.properties.SingleAipFace;
import lombok.AllArgsConstructor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.util.HashMap;


public class AipFaceTemplate {

    private final AipFace client = SingleAipFace.getAipface();

    /**
     * 检测图片中是否包含人脸
     * true:包含  false:不包含
     */
    public boolean detect(String imageUrl){
        // 调用接口
        String image = "https://dlowtanhua001.oss-cn-hangzhou.aliyuncs.com/2022/%E4%B8%83%E6%9C%88/21/91b6d70a-4323-4e6d-a922-a8a5606d5c6f.jpg";
        String imageType = "URL";

        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("face_field", "age");
        options.put("max_face_num", "2");
        options.put("face_type", "LIVE");
        options.put("liveness_control", "LOW");

        // 人脸检测
        JSONObject res = client.detect(imageUrl, imageType, options);
        System.out.println(res.toString(2));

        Integer error_code = (Integer) res.get("error_code");
        return error_code == 0;
    }
}
