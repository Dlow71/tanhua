package com.tanhua.autoconfig.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

//@Configuration
@ConfigurationProperties(prefix = "tanhua.huanxin")
//@Data
public class HuanXinProperties {

    private String appkey;
    private String clientId;
    private String clientSecret;

    public HuanXinProperties() {
    }

    public HuanXinProperties(String appkey, String clientId, String clientSecret) {
        this.appkey = appkey;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }

    public String getAppkey() {
        return appkey;
    }

    public void setAppkey(String appkey) {
        this.appkey = appkey;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }
}