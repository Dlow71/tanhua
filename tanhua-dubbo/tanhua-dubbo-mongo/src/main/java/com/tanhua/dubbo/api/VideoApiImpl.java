package com.tanhua.dubbo.api;

import com.tanhua.dubbo.utils.IdWorker;
import com.tanhua.model.mongo.FocusUser;
import com.tanhua.model.mongo.Video;
import com.tanhua.model.vo.PageResult;
import org.apache.dubbo.config.annotation.DubboService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@DubboService
public class VideoApiImpl implements VideoApi{
    @Autowired
    private MongoTemplate mongoTemplate;
    @Autowired
    private IdWorker idWorker;

    @Override
    public String save(Video video) {
        //补充属性
        video.setVid(idWorker.getNextId("video"));
        video.setCreated(System.currentTimeMillis());
        //保存对象
        mongoTemplate.save(video);
        //返回对象id
        return video.getId().toHexString();
    }

    //根据vid查询视频集合
    @Override
    public List<Video> findByVids(List<Long> vids) {
        Query query = Query.query(Criteria.where("vid").in(vids));
        return mongoTemplate.find(query,Video.class);
    }

    //分页查询视频集合
    @Override
    public List<Video> queryVideoList(int page, Integer pagesize) {
        //分页查询，还要根据发布视频的时间进行排序
        Query query = new Query().limit(pagesize).skip((page-1)*pagesize)
                .with(Sort.by(Sort.Order.desc("created")));
        return mongoTemplate.find(query,Video.class);
    }

    @Override
    public Integer queryFocus(Long userId, Long id) {
        Criteria criteria = Criteria.where("userId").is(userId).and("followUserId").is(id);
        Query query = Query.query(criteria);
        boolean exists = mongoTemplate.exists(query, FocusUser.class);
        return exists ? 1 : 0;
    }

    @Override
    public void saveFocus(FocusUser focusUser) {
        focusUser.setId(ObjectId.get());
        focusUser.setCreated(System.currentTimeMillis());
        mongoTemplate.save(focusUser);
    }

    @Override
    public void unFocus(Long userId, Long uid) {
        Criteria criteria = Criteria.where("userId").is(userId).and("followUserId").is(uid);
        mongoTemplate.remove(Query.query(criteria),FocusUser.class);
    }

    //根据Id查询视频分页列表
    @Override
    public PageResult findByVids(Integer page, Integer pagesize, Long userId) {
        Query query = Query.query(Criteria.where("userId").is(userId));
        long count = mongoTemplate.count(query, Video.class);
        query.limit(pagesize).skip((page-1)*pagesize)
                .with(Sort.by(Sort.Order.desc("created")));
        List<Video> list = mongoTemplate.find(query, Video.class);
        PageResult pageResult = new PageResult(page, pagesize, (int) count, list);
        return pageResult;
    }


}
