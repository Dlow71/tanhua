package com.tanhua.dubbo.api;


import cn.hutool.core.collection.CollUtil;
import com.tanhua.model.mongo.UserLocation;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Circle;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metric;
import org.springframework.data.geo.Metrics;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

@DubboService
public class UserLocationApiImpl implements UserLocationApi{
    @Autowired
    private MongoTemplate mongoTemplate;

    //更新地理位置
    @Override
    public Boolean updateLocation(Long userId, Double longitude, Double latitude, String address) {
        try {
            //根据用户id，查询用户位置信息
            Query query = Query.query(Criteria.where("userId").is(userId));
            UserLocation userLocation = mongoTemplate.findOne(query, UserLocation.class);

            if(userLocation==null){
                //不存在就保存用户信息
                userLocation = new UserLocation();
                userLocation.setUserId(userId);
                userLocation.setAddress(address);
                userLocation.setCreated(System.currentTimeMillis());
                userLocation.setUpdated(System.currentTimeMillis());
                userLocation.setLastUpdated(System.currentTimeMillis());
                userLocation.setLocation(new GeoJsonPoint(longitude,latitude));
                mongoTemplate.save(userLocation);
            }else{
                //存在就更新
                Update update = Update.update("location", new GeoJsonPoint(longitude, latitude))
                        .set("updated", System.currentTimeMillis())
                        .set("lastUpdated", userLocation.getUpdated());
                mongoTemplate.updateFirst(query,update,UserLocation.class);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //查询附近所有人的用户id
    @Override
    public List<Long> queryNearUsers(Long userId, Double valueOf) {
        //根据用户id，查询用户的地理位置
        Query query = Query.query(Criteria.where("userId").is(userId));
        UserLocation userLocation = mongoTemplate.findOne(query, UserLocation.class);
        if(userLocation == null) return null;
        //以当前用户位置绘制原点
        GeoJsonPoint geoJsonPoint = userLocation.getLocation();
        //绘制半径
        Distance distance = new Distance(valueOf / 1000, Metrics.KILOMETERS);
        //绘制圆形
        Circle circle = new Circle(geoJsonPoint, distance);
        //查询
        Query query1 = Query.query(Criteria.where("location").withinSphere(circle));
        List<UserLocation> locationList = mongoTemplate.find(query1, UserLocation.class);
        //由于这个集合中的对象含有GeoJsonPoint属性，这个属性不支持序列化，所以要单独抽出id返回
        return CollUtil.getFieldValues(locationList,"userId",Long.class);
    }
}
