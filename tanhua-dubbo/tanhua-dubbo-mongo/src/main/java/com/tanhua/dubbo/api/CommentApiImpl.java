package com.tanhua.dubbo.api;

import com.tanhua.model.enums.CommentType;
import com.tanhua.model.mongo.Comment;
import com.tanhua.model.mongo.Movement;
import org.apache.dubbo.config.annotation.DubboService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

@DubboService
public class CommentApiImpl implements CommentApi{
    @Autowired
    private MongoTemplate mongoTemplate;

    //发布评论，并获取评论数量
    @Override
    public Integer save(Comment comment1) {
        //查询动态
        Movement movement = mongoTemplate.findById(comment1.getPublishId(), Movement.class);
        //向comment对象设置被评论人属性
        if(movement != null){
            comment1.setPublishUserId(movement.getUserId());
        }
        //保存到数据库
        mongoTemplate.save(comment1);
        //更新动态表中的对应字段
        Query query = Query.query(Criteria.where("id").is(comment1.getPublishId()));
        Update update = new Update();

        if(comment1.getCommentType() == CommentType.LIKE.getType()){
            //点赞
            update.inc("likeCount",1);
        }else if(comment1.getCommentType() == CommentType.COMMENT.getType()){
            //评论
            update.inc("commentCount",1);
        }else{
            //喜欢
            update.inc("loveCount",1);
        }
        //设置更新参数
        FindAndModifyOptions options = new FindAndModifyOptions();
        options.returnNew(true);
        //获取最新的评论数量并返回
        Movement modify = mongoTemplate.findAndModify(query, update, options, Movement.class);
        return modify.statisCount(comment1.getCommentType());
//        if(comment1.getCommentType() == CommentType.LIKE.getType()){
//            //点赞
//            return modify.getLikeCount();
//        }else if(comment1.getCommentType() == CommentType.COMMENT.getType()){
//            //评论
//            return modify.getCommentCount();
//        }else{
//            //喜欢
//            return modify.getLoveCount();
//        }
    }

    @Override
    public List<Comment> findComments(String movementId, CommentType comment, Integer page, Integer pageSize) {
        //1、构造查询条件
        Criteria criteria = Criteria.where("publishId").is(new ObjectId(movementId)).
                and("commentType").is(comment.getType());
        Query query = Query.query(criteria).skip((page - 1) * pageSize).limit(pageSize)
                .with(Sort.by(Sort.Order.desc("created")));
        //2、查询并返回
        return mongoTemplate.find(query,Comment.class);
    }

    @Override
    public Boolean hasComment(String momentId, Long userId, CommentType like) {
        Criteria criteria = Criteria.where("userId").is(userId)
                .and("publishId").is(new ObjectId(momentId))
                .and("commentType").is(like.getType());
        Query query = Query.query(criteria);
        return mongoTemplate.exists(query,Comment.class); //判断数据是否存在
    }

    //删除
    @Override
    public Integer remove(Comment comment) {
        //删除comment表数据
        Criteria criteria = Criteria.where("userId").is(comment.getUserId())
                .and("publishId").is(comment.getPublishId())
                .and("commentType").is(comment.getCommentType());
        Query query = Query.query(criteria);
        mongoTemplate.remove(query,Comment.class);
        //修改动态表中的数量
        Query Moment_query = Query.query(Criteria.where("id").is(comment.getPublishId()));
        Update update = new Update();

        if(comment.getCommentType() == CommentType.LIKE.getType()){
            //点赞
            update.inc("likeCount",-1);
        }else if(comment.getCommentType() == CommentType.COMMENT.getType()){
            //评论
            update.inc("commentCount",-1);
        }else{
            //喜欢
            update.inc("loveCount",-1);
        }
        //设置更新参数
        FindAndModifyOptions options = new FindAndModifyOptions();
        options.returnNew(true);
        //获取最新的评论数量并返回
        Movement modify = mongoTemplate.findAndModify(Moment_query, update, options, Movement.class);
        return modify.statisCount(comment.getCommentType());
    }
}
