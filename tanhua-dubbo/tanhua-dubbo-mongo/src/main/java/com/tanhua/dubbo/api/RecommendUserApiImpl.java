package com.tanhua.dubbo.api;

import cn.hutool.core.collection.CollUtil;
import com.tanhua.model.mongo.RecommendUser;
import com.tanhua.model.mongo.UserLike;
import com.tanhua.model.vo.PageResult;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@DubboService
public class RecommendUserApiImpl implements RecommendUserApi{
    @Autowired
    private MongoTemplate mongoTemplate;

    //查询今日佳人
    @Override
    public RecommendUser queryWithMaxScore(Long toUserId) {
        //根据toUserId查询，按score排序，获取第一条
        //构建criteria
        Criteria criteria = Criteria.where("toUserId").is(toUserId);
        //构建query对象
        Query query = Query.query(criteria).with(Sort.by(Sort.Order.desc("score")))
                .limit(1);
        //调用mongoTemplate查询
        return mongoTemplate.findOne(query,RecommendUser.class);
    }

    @Override                                                  //注意这个userId指定的被推送人
    public PageResult queryRecommendUserList(Integer page, Integer pagesize, Long toUserId) {
        //构建Criteria对象
        Criteria criteria = Criteria.where("toUserId").is(toUserId);
        //构建query对象
        Query query = Query.query(criteria);
        //查询总数，以便于做分页
        long count = mongoTemplate.count(query, RecommendUser.class);
        //查询数据列表
        query.with(Sort.by(Sort.Order.desc("score"))).limit((page-1)*pagesize).skip(pagesize);
        List<RecommendUser> recommendUsers = mongoTemplate.find(query, RecommendUser.class);
        //构造返回值
        return new PageResult(page,pagesize, (int) count,recommendUsers);
    }

    @Override
    public RecommendUser queryByUserId(Long userId, Long toUserId) {
        Criteria criteria = Criteria.where("toUserId").is(toUserId).and("userId").is(userId);
        RecommendUser recommendUser = mongoTemplate.findOne(Query.query(criteria), RecommendUser.class);
        //这个主要是避免返回Null，然后前端数据不好看，所以就默认设置一些参数进去
        if(recommendUser==null){
            recommendUser = new RecommendUser();
            recommendUser.setUserId(userId);
            recommendUser.setToUserId(toUserId);
            //构建缘分值
            recommendUser.setScore(95d);
        }
        return recommendUser;
    }

    /**
     * 1、排除喜欢，不喜欢的用户
     * 2、随机展示
     * 3、指定数量
     */
    @Override
    public List<RecommendUser> queryCardList(Long userId, int count) {
        //1、查询喜欢或者不喜欢的用户id
        List<UserLike> userLikes = mongoTemplate.find(Query.query(Criteria.where("userId").is(userId)), UserLike.class);
        List<Long> ids = CollUtil.getFieldValues(userLikes, "likeUserId", Long.class);
        //2、构造查询推荐用户的条件
        Criteria criteria = Criteria.where("toUserId").is(userId).and("userId").nin(ids);
        //3、使用统计函数，随机获取推荐的用户列表
        TypedAggregation<RecommendUser> aggregation = TypedAggregation.newAggregation(RecommendUser.class,
                Aggregation.match(criteria),//指定查询条件
                Aggregation.sample(count));
        AggregationResults<RecommendUser> results = mongoTemplate.aggregate(aggregation, RecommendUser.class);
        //4、构造返回
        return results.getMappedResults();
    }
}
