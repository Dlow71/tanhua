package com.tanhua.dubbo.api;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tanhua.dubbo.mappers.UserInfoMapper;
import com.tanhua.model.domain.User;
import com.tanhua.model.domain.UserInfo;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;

@DubboService
public class UserInfoImpl implements UserInfoApi {
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public void save(UserInfo userInfo) {
        userInfoMapper.insert(userInfo);
    }

    @Override
    public void update(UserInfo userInfo) {
        userInfoMapper.updateById(userInfo);
    }

    @Override
    public UserInfo findById(Long id) {
        return userInfoMapper.selectById(id);
    }

    @Override
    public Map<Long, UserInfo> findByIds(List<Long> userIds, UserInfo userInfo) {
        LambdaQueryWrapper<UserInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(UserInfo::getId,userIds);
        if(userInfo!=null){
            if(userInfo.getAge()!=null){
                wrapper.lt(UserInfo::getAge,userInfo.getAge());
            }
            if(!StringUtils.isEmpty(userInfo.getGender())){
                wrapper.eq(UserInfo::getGender,userInfo.getGender());
            }
            if(!StringUtils.isEmpty(userInfo.getNickname())){
                wrapper.like(UserInfo::getNickname,userInfo.getNickname());
            }
        }
        List<UserInfo> list = userInfoMapper.selectList(wrapper);
        Map<Long, UserInfo> userInfoMap = CollUtil.fieldValueMap(list, "id");
        return userInfoMap;
    }

    @Override
    public IPage findAll(Integer page, Integer pagesize) {
        return userInfoMapper.selectPage(new Page<UserInfo>(page,pagesize),null);
    }
}
