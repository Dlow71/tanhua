package com.tanhua.dubbo.api;

import com.tanhua.model.mongo.Movement;
import com.tanhua.model.vo.PageResult;

import java.util.List;

public interface MomentApi {
    String publish(Movement movement);

    //根据用户id，查询此用户发的动态
    PageResult findByUserId(Long userId, Integer page, Integer pageSize);

    //根据用户id，查询用户好友发布的动态列表
    List<Movement> findFriendMovements(Integer page, Integer pageSize, Long userId);

    //随机构造10条动态数据
    List<Movement> randomMoments(Integer pageSize);

    //根据pids查询10条动态数据
    List<Movement> findMovementsByPids(List<Long> pids);

    //修改动态审核状态
    Movement findById(String momentId);

    PageResult findByUserId(Long uid, Integer state, Integer page, Integer pagesize);

    void update(String movementId, Integer state);
}
