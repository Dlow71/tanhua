package com.tanhua.dubbo.api;

import com.tanhua.model.mongo.FocusUser;
import com.tanhua.model.mongo.Video;
import com.tanhua.model.vo.PageResult;

import java.util.List;

public interface VideoApi {
    String save(Video video);

    List<Video> findByVids(List<Long> vids);

    List<Video> queryVideoList(int i, Integer pagesize);

    Integer queryFocus(Long userId, Long id);

    void saveFocus(FocusUser focusUser);


    void unFocus(Long userId, Long uid);

    PageResult findByVids(Integer page, Integer pagesize, Long userId);
}
