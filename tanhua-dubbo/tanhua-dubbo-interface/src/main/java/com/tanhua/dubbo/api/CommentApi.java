package com.tanhua.dubbo.api;

import com.tanhua.model.enums.CommentType;
import com.tanhua.model.mongo.Comment;

import java.util.List;

public interface CommentApi {
    //发布评论，并获取评论数量
    Integer save(Comment comment1);

    //分页查询评论
    List<Comment> findComments(String movementId, CommentType comment, Integer page, Integer pageSize);

    Boolean hasComment(String momentId, Long userId, CommentType like);

    Integer remove(Comment comment);
}
