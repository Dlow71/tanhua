package com.tanhua.dubbo.api;

import com.tanhua.model.domain.Settings;

public interface SettingsApi {
    Settings findByUser(Long userId);

    //保存
    void save(Settings settings);

    //更新
    void update(Settings settings);
}
