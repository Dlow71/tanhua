package com.tanhua.dubbo.api;

import com.tanhua.model.domain.Question;

public interface QuestionApi {
    //根据userID查找陌生人问题
    Question findByUser(Long userId);

    void save(Question question);

    void update(Question question);
}
