package com.tanhua.dubbo.api;

import com.tanhua.model.mongo.Visitors;

import java.util.List;

public interface VistorsApi {
    //保存访客数据
    void save(Visitors visitors);

    List<Visitors> queryMyVisitors(Long date, Long userId);
}
