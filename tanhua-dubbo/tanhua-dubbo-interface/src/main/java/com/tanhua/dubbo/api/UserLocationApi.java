package com.tanhua.dubbo.api;

import java.util.List;

public interface UserLocationApi {
    Boolean updateLocation(Long userId, Double longitude, Double latitude, String address);

    //查询附近所有人的用户id
    List<Long> queryNearUsers(Long userId, Double valueOf);
}
