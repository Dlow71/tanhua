package com.tanhua.server.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.PageUtil;
import com.aliyuncs.utils.StringUtils;
import com.github.tobato.fastdfs.domain.conn.FdfsWebServer;
import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.tanhua.autoconfig.template.OssTemplate;
import com.tanhua.commons.utils.Constants;
import com.tanhua.dubbo.api.UserInfoApi;
import com.tanhua.dubbo.api.VideoApi;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.mongo.FocusUser;
import com.tanhua.model.mongo.Video;
import com.tanhua.model.vo.ErrorResult;
import com.tanhua.model.vo.PageResult;
import com.tanhua.model.vo.VideoVo;
import com.tanhua.server.exception.BusinessException;
import com.tanhua.server.interceptor.UserHolder;
import io.netty.util.internal.StringUtil;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SmallVideosService {
    @Autowired
    private FastFileStorageClient client;
    @Autowired
    private FdfsWebServer webServer;
    @Autowired
    private OssTemplate ossTemplate;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @DubboReference
    private VideoApi videoApi;
    @DubboReference
    private UserInfoApi userInfoApi;
    /**
     * 上传视频
     * @param videoTanhuaNail 封面图片文件
     * @param videoFile 视频文件
     */
    public void saveVideos(MultipartFile videoTanhuaNail, MultipartFile videoFile) throws IOException {
        if(videoTanhuaNail.isEmpty()||videoFile.isEmpty()){
            throw new BusinessException(ErrorResult.error());
        }
        //将视频上传到fastDFS上，获取访问url
        String originalFilename = videoFile.getOriginalFilename();
        originalFilename = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
        StorePath storePath = client.uploadFile(videoFile.getInputStream(), videoFile.getSize(), originalFilename, null);
        String videoUrl = webServer.getWebServerUrl() + storePath.getFullPath();
        //将封面图片上传到oss上，获取访问url
        String imageUrl = ossTemplate.upload(videoTanhuaNail.getOriginalFilename(), videoTanhuaNail.getInputStream());
        //构造video对象
        Video video = new Video();
        video.setUserId(UserHolder.getUserId());
        video.setPicUrl(imageUrl);
        video.setVideoUrl(videoUrl);
        video.setText("挺好");
        //调用api保存数据
        String videoId = videoApi.save(video);
        if(StringUtils.isEmpty(videoId)){
            throw new BusinessException(ErrorResult.error());
        }
    }

    //视频列表
    @Cacheable(
            value="videos",
            key = "T(com.tanhua.server.interceptor.UserHolder).getUserId()+'_'+#page+'_'+#pagesize") //userId_page_pageSize
    public PageResult queryVideoList(Integer page, Integer pagesize) {
        //查询redis中的vid数据
        String redisKey = Constants.VIDEO_RECOMMEND + UserHolder.getUserId();
        String redisValue = redisTemplate.opsForValue().get(redisKey);
        //判断数据是否存在，以及redis中的数据是否满足本次分页条数
        List<Video> list = new ArrayList<>();
        //记录redis记录的是多少页
        int redisPages = 0;
        if(!StringUtils.isEmpty(redisValue)){
            //如果数据存在，根据vid查询数据
            String[] split = redisValue.split(",");
            if((page-1)*pagesize< split.length){
                List<Long> vids = Arrays.stream(split).skip((page - 1) * pagesize).limit(pagesize)
                        .map(e -> Long.valueOf(e))
                        .collect(Collectors.toList());
                list = videoApi.findByVids(vids);
            }
            redisPages = PageUtil.totalPage(split.length,pagesize);
        }
        //如果数据不存在，分页查询视频数据
        if(list.isEmpty()){
            //page的计算规则，传入的页码-redis查询的总页数
            list = videoApi.queryVideoList(page-redisPages,pagesize);
        }
        //提取视频列表中，所有的用户Id
        List<Long> userIds = CollUtil.getFieldValues(list, "userId", Long.class);
        //查询用户信息
        Map<Long, UserInfo> infoMap = userInfoApi.findByIds(userIds, null);
        //构建返回值
        List<VideoVo> vos = new ArrayList<>();
        for (Video video : list) {
            UserInfo userInfo = infoMap.get(video.getUserId());
            if(userInfo!=null){
                VideoVo vo = VideoVo.init(userInfo, video);
                String Key = Constants.FOCUS_USER+UserHolder.getUserId();
                String hashKey = video.getUserId() + "";
                Integer value = (Integer) redisTemplate.opsForHash().get(Key,hashKey);
                if(value!=null)
                vo.setHasFocus(value);
                //愚昧
//                vo.setHasFocus(videoApi.queryFocus(UserHolder.getUserId(),userInfo.getId()));
                vos.add(vo);
            }
        }
        return new PageResult(page,pagesize,0,vos);
    }

    //关注用户
    public void focusUser(Long uid) {
        String redisKey = Constants.FOCUS_USER+UserHolder.getUserId();
        String hashKey = uid+"";
        String value = (String) redisTemplate.opsForHash().get(redisKey, hashKey);
        if(value == null || "1".equals(value)) return;
        FocusUser focusUser = new FocusUser();
        focusUser.setUserId(UserHolder.getUserId());
        focusUser.setFollowUserId(uid);
        videoApi.saveFocus(focusUser);

        redisTemplate.opsForHash().put(redisKey,hashKey,"1");
    }

    //取消关注
    public void UnfocusUser(Long uid) {
        videoApi.unFocus(UserHolder.getUserId(),uid);
        String redisKey = Constants.FOCUS_USER+UserHolder.getUserId();
        String hashKey = uid+"";
        redisTemplate.opsForHash().delete(redisKey,hashKey);
    }
}
