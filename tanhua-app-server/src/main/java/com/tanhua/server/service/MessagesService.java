package com.tanhua.server.service;

import cn.hutool.core.collection.CollUtil;
import com.tanhua.autoconfig.template.HuanXinTemplate;
import com.tanhua.commons.utils.Constants;
import com.tanhua.dubbo.api.FriendApi;
import com.tanhua.dubbo.api.UserApi;
import com.tanhua.dubbo.api.UserInfoApi;
import com.tanhua.model.domain.User;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.mongo.Friend;
import com.tanhua.model.vo.ContactVo;
import com.tanhua.model.vo.ErrorResult;
import com.tanhua.model.vo.PageResult;
import com.tanhua.model.vo.UserInfoVo;
import com.tanhua.server.exception.BusinessException;
import com.tanhua.server.interceptor.UserHolder;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class MessagesService {
    @DubboReference
    private UserInfoApi userInfoApi;
    @DubboReference
    private UserApi userApi;
    @DubboReference
    private FriendApi friendApi;
    @Autowired
    private HuanXinTemplate huanXinTemplate;

    //根据环信用户id来查询用户详情
    public UserInfoVo findUserInfoByHuanXin(String huanxinId) {
        //根据环信id查询用户
        User user = userApi.findByHuanXin(huanxinId);
        //根据用户id，查用户详情
        UserInfo userInfo = userInfoApi.findById(user.getId());
        //构建vo返回值
        UserInfoVo vo = new UserInfoVo();
        BeanUtils.copyProperties(userInfo,vo);
        if(userInfo.getAge() != null){
            vo.setAge(userInfo.getAge().toString());
        }
        return vo;
    }

    //添加好友关系
    public void contacts(Long friendId) {
        //1、将好友关系注册到环信
        Boolean aBoolean = huanXinTemplate.addContact(Constants.HX_USER_PREFIX + UserHolder.getUserId(),
                Constants.HX_USER_PREFIX + friendId);
        if(!aBoolean){
            throw new BusinessException(ErrorResult.error());
        }
        //2、如果注册成功，记录好友关系到mongoDB
        friendApi.save(UserHolder.getUserId(),friendId);
    }

    public PageResult findFriends(Integer page, Integer pagesize, String keyword) {
        //1、调用api查询当前用户好友数据 List<friend>
        List<Friend> list = friendApi.findByUserId(UserHolder.getUserId(),page,pagesize);
        if(CollUtil.isEmpty(list)) return new PageResult();
        //2、提取数据列表中的好友id
        List<Long> userIds = CollUtil.getFieldValues(list, "friendId", Long.class);
        //3、调用userInfoAPI查询用户详情
        UserInfo userInfo = new UserInfo();
        userInfo.setNickname(keyword);
        Map<Long, UserInfo> userInfoMap = userInfoApi.findByIds(userIds, userInfo);
        //4、构造vo对象
        List<ContactVo> vos = new ArrayList<>();
        for (Friend friend : list) {
            UserInfo info = userInfoMap.get(friend.getFriendId());
            if(info!=null){
                ContactVo vo = ContactVo.init(info);
                vos.add(vo);
            }
        }
        return new PageResult(page,pagesize,0,vos);
    }
}
