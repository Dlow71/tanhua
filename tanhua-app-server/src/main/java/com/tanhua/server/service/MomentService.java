package com.tanhua.server.service;

import cn.hutool.core.collection.CollUtil;
import com.tanhua.autoconfig.template.OssTemplate;
import com.tanhua.commons.utils.Constants;
import com.tanhua.dubbo.api.MomentApi;
import com.tanhua.dubbo.api.UserInfoApi;
import com.tanhua.dubbo.api.VistorsApi;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.mongo.Movement;
import com.tanhua.model.mongo.Visitors;
import com.tanhua.model.vo.ErrorResult;
import com.tanhua.model.vo.MovementsVo;
import com.tanhua.model.vo.PageResult;
import com.tanhua.model.vo.VisitorsVo;
import com.tanhua.server.exception.BusinessException;
import com.tanhua.server.interceptor.UserHolder;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MomentService {
    @Autowired
    private OssTemplate ossTemplate;
    @DubboReference
    private MomentApi momentApi;
    @DubboReference
    private UserInfoApi userInfoApi;
    @DubboReference
    private VistorsApi vistorsApi;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Autowired
    private MqMessageService mqMessageService;

    //发布动态
    public void publishMoment(Movement movement, MultipartFile[] multipartFile) throws IOException {
        //1、判断发布动态内容是否存在
        if(StringUtils.isEmpty(movement.getTextContent())){
            throw new BusinessException(ErrorResult.contentError());
        }
        //2、获取当前登录用户id
        Long userId = UserHolder.getUserId();
        //3、将文件内容上传到阿里云oss，获取请求地址
        List<String> medias = new ArrayList<>();
        for (MultipartFile file : multipartFile) {
            String upload = ossTemplate.upload(file.getOriginalFilename(), file.getInputStream());
            medias.add(upload);
        }
        //4、将数据封装到moment对象
        movement.setUserId(userId);
        movement.setMedias(medias);
        //5、调用api，发布动态
        String momentId = momentApi.publish(movement);
        //发送动态id到mq进行审核
//        mqMessageService.sendAudiService(momentId);
    }

    //查询个人动态
    public PageResult findByUserId(Long userId, Integer page, Integer pageSize) {
        //1、根据用户id，调用api查询个人动态内容(PageResult -- moment)
        PageResult pr = momentApi.findByUserId(userId,page,pageSize);
        //2、获取pageResult中的item列表对象
        List<Movement> items = (List<Movement>)pr.getItems();
        //3、非空判断
        if(items==null){
            return pr;
        }
        //4、循环数据列表
        UserInfo userInfo = userInfoApi.findById(userId);
        List<MovementsVo> list = new ArrayList<>();
        for (Movement item : items) {
            //5、一个moment对象构建一个vo对象
            MovementsVo vo = MovementsVo.init(userInfo, item);
            list.add(vo);
        }
        //6、构建返回值
        pr.setItems(list);
        return pr;
    }

    //查询好友动态
    public PageResult findFriendMovements(Integer page, Integer pageSize) {
        //1、获取当前用户的id
        Long userId = UserHolder.getUserId();
        //2、调用api查询当前用户好友发布的动态列表
        List<Movement> list = momentApi.findFriendMovements(page,pageSize,userId);
        return getPageResult(page, pageSize, list);
    }

    private PageResult getPageResult(Integer page, Integer pageSize, List<Movement> list) {
        //3、判断列表是否为空
        if(list == null || list.isEmpty()){  //CollUtil.isEmpty()
            return new PageResult();
        }
        //4、提取动态发布人的id列表
//        List<Long> ids = new ArrayList<>();
//        for (Movement movement : list) {
//            ids.add(movement.getUserId());
//        }
        List<Long> userIds = CollUtil.getFieldValues(list, "userId", Long.class);
        //5、根据用户的id列表获取用户详情
        Map<Long, UserInfo> userInfoMap = userInfoApi.findByIds(userIds, null);
        //6、一个moment对象构建一个vo对象
        List<MovementsVo> vos = new ArrayList<>();
        for (Movement movement : list) {
            UserInfo userInfo = userInfoMap.get(movement.getUserId());
            if(userInfo!=null){
                MovementsVo vo = MovementsVo.init(userInfo, movement);
                //修复点赞状态的bug，判断一下hashKey是否存在
                String key = Constants.MOVEMENTS_INTERACT_KEY + movement.getId().toHexString();
                String hashKey = Constants.MOVEMENT_LIKE_HASHKEY + UserHolder.getUserId();
                if(redisTemplate.opsForHash().hasKey(key,hashKey)){
                    vo.setHasLiked(1);
                }
                String loveHashKey = Constants.MOVEMENT_LOVE_HASHKEY + UserHolder.getUserId();
                if(redisTemplate.opsForHash().hasKey(key,hashKey)){
                    vo.setHasLoved(1);
                }
                vos.add(vo);
            }
        }
        //7、构造PageResult
        return new PageResult(page, pageSize, 0, vos);
    }

    //查询推荐动态
    public PageResult findRecommendMovements(Integer page, Integer pageSize) {
        //从redis中去获取推荐数据
        String redisKey = Constants.MOVEMENTS_RECOMMEND +UserHolder.getUserId();
        String redisValue = redisTemplate.opsForValue().get(redisKey);
        List<Movement> list = Collections.EMPTY_LIST;
        //判断推荐数据是否存在
        if(StringUtils.isEmpty(redisValue)){
            //不存在就根据api随机构造10条数据
            list = momentApi.randomMoments(pageSize);
        }else{
            //存在就处理pid数据，调用api根据pid数组查询动态数据
            //10020,28,10092,24,25,27,10064,26,10067,20,16,10099,19,10015,10040,10093,18,17,22,10081
            String[] values = redisValue.split(",");
            //判断当前页的起始条数是否小于数组总数，如果小于就进行分页处理
            //假设page=4 ，pageSize=5，数组长度为15，那此时起始条数刚好是15，也是不需要分页的
            if((page-1)*pageSize < values.length){
                //skip是跳过几个元素 ,limit是包含几个元素
                List<Long> pids = Arrays.stream(values).skip((page - 1) * pageSize).limit(pageSize)
                        .map(e->Long.valueOf(e))
                        .collect(Collectors.toList());
                list = momentApi.findMovementsByPids(pids);
            }
        }
        //调用公共方法，构造返回值
        return getPageResult(page,pageSize,list);
    }

    //根据id查询
    public MovementsVo findById(String momentId) {
        mqMessageService.sendLogService(UserHolder.getUserId(),"0202","movement",null);
        //调用api，根据id查询动态详情
        Movement movement = momentApi.findById(momentId);
        //转换vo对象
        if(movement!=null){
            UserInfo userInfo = userInfoApi.findById(movement.getUserId());
            return MovementsVo.init(userInfo,movement);
        }else{
            return null;
        }
    }

    //首页访客列表
    public List<VisitorsVo> queryVisitorsList() {
        //1、查询访问时间
        String key = Constants.VISITORS_USER;
        String hashKey = String.valueOf(UserHolder.getUserId());
        String value = (String) redisTemplate.opsForHash().get(key,hashKey);
        Long date = StringUtils.isEmpty(value) ? null : Long.valueOf(value);
        //2、调用api查询数据列表  List<Visitors>
        List<Visitors> list = vistorsApi.queryMyVisitors(date,UserHolder.getUserId());
        if(CollUtil.isEmpty(list)){
            return new ArrayList<>();
        }
        //3、提取用户id
        List<Long> userIds = CollUtil.getFieldValues(list, "visitorUserId", Long.class);
        //4、查看用户详情
        Map<Long, UserInfo> infoMap = userInfoApi.findByIds(userIds,null);
        //5、构造返回
        List<VisitorsVo> vos = new ArrayList<>();
        for (Visitors visitors : list) {
            UserInfo userInfo = infoMap.get(visitors.getVisitorUserId());
            if(userInfo!=null){
                VisitorsVo vo = VisitorsVo.init(userInfo, visitors);
                vos.add(vo);
            }
        }
        return vos;
    }
}
