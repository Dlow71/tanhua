package com.tanhua.server.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tanhua.dubbo.api.BlackListApi;
import com.tanhua.dubbo.api.QuestionApi;
import com.tanhua.dubbo.api.SettingsApi;
import com.tanhua.model.domain.Question;
import com.tanhua.model.domain.Settings;
import com.tanhua.model.vo.PageResult;
import com.tanhua.model.vo.SettingsVo;
import com.tanhua.server.interceptor.UserHolder;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class SettingsService {
    @DubboReference
    private QuestionApi questionApi;

    @DubboReference
    private SettingsApi settingsApi;

    @DubboReference
    private BlackListApi blackListApi;

    public SettingsVo getQuestions() {
        SettingsVo settingsVo = new SettingsVo();
        //获取用户id
        Long userId = UserHolder.getUserId();
        settingsVo.setId(userId);
        //获取用户手机号码
        String mobile = UserHolder.getMobile();
        settingsVo.setPhone(mobile);
        //获取用户的陌生人问题
        Question question = questionApi.findByUser(userId);
        String txt = question == null ? "你今天锻炼了吗？" : question.getTxt();
        settingsVo.setStrangerQuestion(txt);
        //获取用户的APP通知开关
        Settings settings = settingsApi.findByUser(userId);
        if(settings != null){
            settingsVo.setGonggaoNotification(settings.getGonggaoNotification());
            settingsVo.setLikeNotification(settings.getLikeNotification());
            settingsVo.setPinglunNotification(settings.getPinglunNotification());
        }
        return settingsVo;
    }

    //设置陌生人问题
    public void saveQuestion(String content) {
        //获取当前用户的api
        Long userId = UserHolder.getUserId();
        //调用api查询当前用户的陌生人问题
        Question question = questionApi.findByUser(userId);
        //判断问题是否存在
        if(question==null){
            //如果不存在就直接插入
            question = new Question();
            question.setUserId(userId);
            question.setTxt(content);
            questionApi.save(question);
        }else{
            //如果存在做修改动作
            question.setTxt(content);
            questionApi.update(question);
        }




    }

    /**
     * 通知设置
     * @param map
     */
    public void saveSettings(Map map) {
        boolean like = (boolean) map.get("likeNotification");
        boolean pinglun = (boolean) map.get("pinglunNotification");
        boolean gonggao = (boolean) map.get("gonggaoNotification");
        //获取当前用户id
        Long userId = UserHolder.getUserId();
        //根据用户id，查询用户的通知设置
        Settings settings = settingsApi.findByUser(userId);
        if(settings==null){
            //不存在就保存
            settings = new Settings();
            settings.setUserId(userId);
            settings.setLikeNotification(like);
            settings.setGonggaoNotification(gonggao);
            settings.setPinglunNotification(pinglun);
            settingsApi.save(settings);
        }else{
            //存在就更新
            settings.setLikeNotification(like);
            settings.setGonggaoNotification(gonggao);
            settings.setPinglunNotification(pinglun);
            settingsApi.update(settings);
        }
    }

    //分页查询黑名单列表
    public PageResult blacklist(int page, int size) {
        //获取当前用户id
        Long userId = UserHolder.getUserId();
        //调用api查询用户黑名单列表   IPage
        IPage iPage = blackListApi.findByUserId(userId,page,size);
        //对象转化，将查询到的Ipage对象封装到pageResult中
        PageResult pageResult = new PageResult(page,size, (int) iPage.getTotal(),iPage.getRecords());
        //返回pageResult
        return pageResult;
    }

    //取消黑名单
    public void deleteBlackList(Long blakcUserId) {
        //获取当前用户id
        Long userId = UserHolder.getUserId();
        //调用api删除
        blackListApi.delete(userId,blakcUserId);
    }
}
