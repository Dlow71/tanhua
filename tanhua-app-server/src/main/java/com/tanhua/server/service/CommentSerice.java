package com.tanhua.server.service;

import cn.hutool.core.collection.CollUtil;
import com.tanhua.commons.utils.Constants;
import com.tanhua.dubbo.api.CommentApi;
import com.tanhua.dubbo.api.UserInfoApi;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.enums.CommentType;
import com.tanhua.model.mongo.Comment;
import com.tanhua.model.vo.CommentVo;
import com.tanhua.model.vo.ErrorResult;
import com.tanhua.model.vo.PageResult;
import com.tanhua.server.exception.BusinessException;
import com.tanhua.server.interceptor.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class CommentSerice {
    @DubboReference
    private CommentApi commentApi;
    @DubboReference
    private UserInfoApi userInfoApi;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    //发布评论
    public void saveComments(String movementId, String comment) {
        //获取操作用户id
        Long userId = UserHolder.getUserId();
        //构造comment对象
        Comment comment1 = new Comment();
        comment1.setPublishId(new ObjectId(movementId));
        comment1.setCommentType(CommentType.COMMENT.getType());
        comment1.setContent(comment);
        comment1.setUserId(userId);
        comment1.setCreated(System.currentTimeMillis());
        //调用api保存评论
        Integer CommentCount = commentApi.save(comment1);
        System.out.println("CommentCount="+CommentCount);
    }

    //分页查询动态下的评论
    public PageResult findComments(Integer page, Integer pageSize, String movementId) {
        //调用api，查询指定movementId且commentType为2的评论
        List<Comment> list = commentApi.findComments(movementId,CommentType.COMMENT,page,pageSize);
        //判断评论是否存在
        if(CollUtil.isEmpty(list)){
            return new PageResult();
        }
        //提取出用户id,调用api查询用户详情
        List<Long> userIds = CollUtil.getFieldValues(list, "userId", Long.class);
        Map<Long, UserInfo> infoMap = userInfoApi.findByIds(userIds, null);
        //构造vo对象
        List<CommentVo> vos = new ArrayList<>();
        for (Comment comment : list) {
            UserInfo userInfo = infoMap.get(comment.getUserId());
            if(userInfo!=null){
                CommentVo commentVo = CommentVo.init(userInfo, comment);
                vos.add(commentVo);
            }
        }
        //构造返回值
        return new PageResult(page,pageSize,0,vos);
    }

    //动态点赞
    public Integer likeComment(String momentId) {
        //调用api查询用户是否点赞
        Boolean hasComment = commentApi.hasComment(momentId,UserHolder.getUserId(),CommentType.LIKE);
        //已经点赞就抛出异常
        if(hasComment){
            throw new BusinessException(ErrorResult.likeError());
        }
        //调用api保存到mongoDB
        Comment comment = new Comment();
        comment.setPublishId(new ObjectId(momentId));
        comment.setCommentType(CommentType.LIKE.getType());
        comment.setUserId(UserHolder.getUserId());
        comment.setCreated(System.currentTimeMillis());
        Integer count = commentApi.save(comment);
        //拼接redisKey,将用户点赞状态存入redis
        String key = Constants.MOVEMENTS_INTERACT_KEY + momentId;
        String hashKey = Constants.MOVEMENT_LIKE_HASHKEY + UserHolder.getUserId();
        redisTemplate.opsForHash().put(key,hashKey,"1");
        //这个返回的是最新点赞数
        return count;
    }

    //取消点赞
    public Integer dislikeComment(String momentId) {
        //调用api查询用户是否点赞
        Boolean hasComment = commentApi.hasComment(momentId,UserHolder.getUserId(),CommentType.LIKE);
        //如果未点赞，抛出异常
        if(!hasComment){
            throw new BusinessException(ErrorResult.disLikeError());
        }
        //如果已经点赞，调用api删除数据，返回点赞数量
        Comment comment = new Comment();
        comment.setPublishId(new ObjectId(momentId));
        comment.setCommentType(CommentType.LIKE.getType());
        comment.setUserId(UserHolder.getUserId());
        Integer count = commentApi.remove(comment);
        //拼接redis中的key和hashKey，删除点赞状态
        String key = Constants.MOVEMENTS_INTERACT_KEY + momentId;
        String hashKey = Constants.MOVEMENT_LIKE_HASHKEY + UserHolder.getUserId();
        redisTemplate.opsForHash().delete(key,hashKey);
        return count;
    }

    //喜欢
    public Integer loveComment(String momentId) {
        //调用api查询用户是否点赞
        Boolean hasComment = commentApi.hasComment(momentId,UserHolder.getUserId(),CommentType.LOVE);
        //已经喜欢就抛出异常
        if(hasComment){
            throw new BusinessException(ErrorResult.likeError());
        }
        //调用api保存到mongoDB
        Comment comment = new Comment();
        comment.setPublishId(new ObjectId(momentId));
        comment.setCommentType(CommentType.LOVE.getType());
        comment.setUserId(UserHolder.getUserId());
        comment.setCreated(System.currentTimeMillis());
        Integer count = commentApi.save(comment);
        //拼接redisKey,将用户点赞状态存入redis
        String key = Constants.MOVEMENTS_INTERACT_KEY + momentId;
        String hashKey = Constants.MOVEMENT_LOVE_HASHKEY + UserHolder.getUserId();
        redisTemplate.opsForHash().put(key,hashKey,"1");
        //这个返回的是最新点赞数
        return count;
    }

    //取消喜欢
    public Integer disloveComment(String momentId) {
        //调用api查询用户是否点赞
        Boolean hasComment = commentApi.hasComment(momentId,UserHolder.getUserId(),CommentType.LOVE);
        //如果未点赞，抛出异常
        if(!hasComment){
            throw new BusinessException(ErrorResult.disloveError());
        }
        //如果已经点赞，调用api删除数据，返回点赞数量
        Comment comment = new Comment();
        comment.setPublishId(new ObjectId(momentId));
        comment.setCommentType(CommentType.LOVE.getType());
        comment.setUserId(UserHolder.getUserId());
        Integer count = commentApi.remove(comment);
        //拼接redis中的key和hashKey，删除点赞状态
        String key = Constants.MOVEMENTS_INTERACT_KEY + momentId;
        String hashKey = Constants.MOVEMENT_LOVE_HASHKEY + UserHolder.getUserId();
        redisTemplate.opsForHash().delete(key,hashKey);
        return count;
    }
}
