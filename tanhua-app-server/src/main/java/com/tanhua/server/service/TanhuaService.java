package com.tanhua.server.service;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import com.tanhua.autoconfig.template.HuanXinTemplate;
import com.tanhua.commons.utils.Constants;
import com.tanhua.dubbo.api.*;
import com.tanhua.model.domain.Question;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.dto.RecommendUserDto;
import com.tanhua.model.mongo.RecommendUser;
import com.tanhua.model.mongo.Visitors;
import com.tanhua.model.vo.ErrorResult;
import com.tanhua.model.vo.NearUserVo;
import com.tanhua.model.vo.PageResult;
import com.tanhua.model.vo.TodayBest;
import com.tanhua.server.exception.BusinessException;
import com.tanhua.server.interceptor.UserHolder;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TanhuaService {
    @DubboReference
    private RecommendUserApi recommendUserApi;
    @DubboReference
    private UserInfoApi userInfoApi;
    @DubboReference
    private QuestionApi questionApi;
    @DubboReference
    private UserLikeApi userLikeApi;
    @DubboReference
    private UserLocationApi userLocationApi;
    @DubboReference
    private VistorsApi vistorsApi;
    @Autowired
    private HuanXinTemplate huanXinTemplate;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    @Autowired
    private MessagesService messagesService;
    @Value("tanhua.default.recommend.users")
    private String recommendUser;


    //查询今日佳人数据
    public TodayBest todayBest() {
        Long userId = UserHolder.getUserId();
        RecommendUser recommendUser = recommendUserApi.queryWithMaxScore(userId);
        if(recommendUser == null){
            recommendUser = new RecommendUser();
            recommendUser.setUserId(1L);
            recommendUser.setScore(99d);
        }
        //根据recommendUser被推荐人的id，查询这个人的详情
        UserInfo userInfo = userInfoApi.findById(recommendUser.getUserId());
        TodayBest vo =TodayBest.init(userInfo,recommendUser);
        return vo;
    }

    //查询分页推荐好友列表
//    public PageResult recommendation(RecommendUserDto dto) {
//        //获取用户id
//        Long userId = UserHolder.getUserId();
//        //调用recommendUserApi 分页查询数据列表(PageResult --> RecommendUser)
//        PageResult pr = recommendUserApi.queryRecommendUserList(dto.getPage(),dto.getPagesize(),userId);
//        //获取分页当中的RecommendUser数据列表
//        List<RecommendUser> items = (List<RecommendUser>)pr.getItems();
//        //判断列表是否为空
//        if(items==null) return pr;
//        //循环RecommendUser数据列表，根据推荐的用户id查询数据详情
//        List<TodayBest> list = new ArrayList<>();
//        for (RecommendUser item : items) {
//            Long recommendUserId = item.getUserId();
//            UserInfo userInfo = userInfoApi.findById(recommendUserId);
//            if(userInfo!=null){
//                //根据筛选条件判断是否需要加入list集合,第一个判断怀疑有问题
//                if(!StringUtils.isEmpty(dto.getGender()) && !dto.getGender().equals(userInfo.getGender())){
//                    continue;
//                }
//                if(dto.getAge() != null && dto.getAge()<userInfo.getAge()){
//                    continue;
//                }
//                TodayBest todayBest = TodayBest.init(userInfo, item);
//                list.add(todayBest);
//            }
//        }
//        pr.setItems(list);
//        //构造返回值
//        return pr;
//    }

    //优化查询分页推荐好友列表
    public PageResult recommendation(RecommendUserDto dto) {
        //获取用户id
        Long userId = UserHolder.getUserId();
        //调用recommendUserApi 分页查询数据列表(PageResult --> RecommendUser)
        PageResult pr = recommendUserApi.queryRecommendUserList(dto.getPage(), dto.getPagesize(), userId);
        //获取分页当中的RecommendUser数据列表
        List<RecommendUser> items = (List<RecommendUser>) pr.getItems();
        //判断列表是否为空
        if (items == null) return pr;
        //从items中提取出所有推荐的id列表
        List<Long> ids = CollUtil.getFieldValues(items, "userId", Long.class);
        //构建查询条件，批量查询所有的用户详情
        UserInfo userInfo = new UserInfo();
        userInfo.setAge(dto.getAge());
        userInfo.setGender(dto.getGender());
        Map<Long, UserInfo> map = userInfoApi.findByIds(ids, userInfo);
        //循环推荐的数据列表，构建vo对象
        List<TodayBest> list = new ArrayList<>();
        for (RecommendUser item : items) {
            UserInfo info = map.get(item.getUserId());
            if(info!=null){
                TodayBest todayBest = TodayBest.init(info, item);
                list.add(todayBest);
            }
        }
        //构造返回
        pr.setItems(list);
//        //构造返回值
        return pr;
    }

    public TodayBest personalInfo(Long userId) {
        //根据用户id，查询用户详情
        UserInfo userInfo = userInfoApi.findById(userId);
        //根据操作人id和查看的用户id，查询两者的推荐数据
        RecommendUser recommendUser = recommendUserApi.queryByUserId(userId,UserHolder.getUserId());
        //构造访客数据，调用api保存
        Visitors visitors = new Visitors();
        visitors.setUserId(userId);
        visitors.setVisitorUserId(UserHolder.getUserId());
        visitors.setFrom("首页");
        visitors.setDate(System.currentTimeMillis());
        visitors.setVisitDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
        visitors.setScore(recommendUser.getScore());
        vistorsApi.save(visitors);
        //构造返回值
        return TodayBest.init(userInfo,recommendUser);
    }

    //查看陌生人问题
    public String stangerQuestions(Long userId) {
        Question question = questionApi.findByUser(userId);
        return question == null ? "打咩哟" : question.getTxt();
    }


    //回复陌生人问题
    public void replyQuestions(Long userId, String reply) {
        //1、构造消息数据
        Long currentUserId = UserHolder.getUserId();
        UserInfo userInfo = userInfoApi.findById(currentUserId);
        Map map = new HashMap<>();
        map.put("userId",currentUserId);
        map.put("huanXinId", Constants.HX_USER_PREFIX+currentUserId);
        map.put("nickname",userInfo.getNickname());
        map.put("strangerQuestion",stangerQuestions(userId));
        map.put("reply",reply);
        //将map转为json形式
        String message = JSONUtil.toJsonStr(map);
        //2、调用template对象，发送消息  接收方环信id  消息
        Boolean aBoolean = huanXinTemplate.sendMsg(Constants.HX_USER_PREFIX + userId, message);
        if(!aBoolean){
            throw new BusinessException(ErrorResult.error());
        }
    }

    //推荐用户列表
    public List<TodayBest> queryCardsList() {
        //调用推荐api查询数据列表(排除喜欢/不喜欢的用户，还要有数量限制)
        List<RecommendUser> users = recommendUserApi.queryCardList(UserHolder.getUserId(),10);
        //判断数据是否存在，不存在就构造默认数据
        if(CollUtil.isEmpty(users)){
            users = new ArrayList<>();
            String[] userIds = recommendUser.split(",");
            for (String userId : userIds) {
                RecommendUser recommendUser = new RecommendUser();
                recommendUser.setUserId(Convert.toLong(userId));
                recommendUser.setToUserId(UserHolder.getUserId());
                recommendUser.setScore(RandomUtil.randomDouble(60, 90));
                users.add(recommendUser);
            }
        }
        //存在就构造vo
        List<Long> ids = CollUtil.getFieldValues(users, "userId", Long.class);
        Map<Long, UserInfo> infoMap = userInfoApi.findByIds(ids, null);
        List<TodayBest> vos = new ArrayList<>();
        for (RecommendUser user : users) {
            UserInfo userInfo = infoMap.get(user.getUserId());
            if(userInfo!=null){
                TodayBest todayBest = TodayBest.init(userInfo, user);
                vos.add(todayBest);
            }
        }
        return vos;
    }

    public void likeUser(Long likeUserId) {
        //调用api保存喜欢数据
        Boolean save = userLikeApi.saveOrUpdate(UserHolder.getUserId(),likeUserId,true);
        if(!save){
            throw new BusinessException(ErrorResult.error());
        }
        //操作redis，先删除不喜欢数据（无需判断是否存在，直接删不报错），再写入喜欢数据，可以有返回值（返回删除个数）
        redisTemplate.opsForSet().remove(Constants.USER_NOT_LIKE_KEY + UserHolder.getUserId(), likeUserId.toString());
        redisTemplate.opsForSet().add(Constants.USER_NOT_LIKE_KEY + UserHolder.getUserId(), likeUserId.toString());
        //判断是否双向喜欢
        if(isLike(likeUserId,UserHolder.getUserId())){
            //是双向喜欢就添加好友
            messagesService.contacts(likeUserId);
        }

    }

    public Boolean isLike(Long userId,Long likeUserId){
        String key = Constants.USER_NOT_LIKE_KEY + userId;
        return redisTemplate.opsForSet().isMember(key,likeUserId.toString());
    }

    //不喜欢
    public void notLikeUser(Long likeUserId) {
        //调用api保存不喜欢数据
        Boolean save = userLikeApi.saveOrUpdate(UserHolder.getUserId(),likeUserId,false);
        if(!save){
            throw new BusinessException(ErrorResult.error());
        }
        redisTemplate.opsForSet().add(Constants.USER_NOT_LIKE_KEY + UserHolder.getUserId(), likeUserId.toString());
        redisTemplate.opsForSet().remove(Constants.USER_NOT_LIKE_KEY + UserHolder.getUserId(), likeUserId.toString());
    }

    //搜附近
    public List<NearUserVo> queryNearUser(String gender, String distance) {
        //调用api查询附近的用户(返回的是附近人的所有id，会包含当前用户的id，注意处理)
        List<Long> ids = userLocationApi.queryNearUsers(UserHolder.getUserId(),Double.valueOf(distance));
        //判断集合是否为空
        if(CollUtil.isEmpty(ids)){
            return new ArrayList<>();
        }
        //调用userInfoApi根据id查询用户详情
        UserInfo userInfo = new UserInfo();
        userInfo.setGender(gender);
        Map<Long, UserInfo> infoMap = userInfoApi.findByIds(ids, userInfo);
        //构造返回值
        List<NearUserVo> vos = new ArrayList<>();
        for (Long id : ids) {
            if(id==UserHolder.getUserId()){
                continue;
            }
            UserInfo info = infoMap.get(id);
            if(info!=null){
                NearUserVo vo = NearUserVo.init(info);
                vos.add(vo);
            }
        }
        return vos;
    }
}
