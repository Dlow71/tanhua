package com.tanhua.server.interceptor;

import com.tanhua.model.domain.User;

/**
 * 工具类：向threadLocal存储数据的方法
 */
public class UserHolder {
    private static ThreadLocal<User> t1 = new ThreadLocal<User>();

    public static void set(User user){
        t1.set(user);
    }

    //获取用户对象
    public static User get(){
        return t1.get();
    }

    //获取用户id
    public static Long getUserId(){
        return t1.get().getId();
    }

    //获取用户手机号
    public static String getMobile(){
        return t1.get().getMobile();
    }

    //清空
    public static void remove(){
        t1.remove();
    }
}
