package com.tanhua.server.exception;


import com.tanhua.model.vo.ErrorResult;
import lombok.Data;

@Data
public class BusinessException extends RuntimeException{

    private ErrorResult errorResult;

    public ErrorResult getErrorResult() {
        return errorResult;
    }

    public void setErrorResult(ErrorResult errorResult) {
        this.errorResult = errorResult;
    }

    public BusinessException(ErrorResult errorResult){
        super(errorResult.getErrMessage());
        this.errorResult = errorResult;
    }
}
