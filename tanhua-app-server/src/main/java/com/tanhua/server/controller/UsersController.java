package com.tanhua.server.controller;

import com.tanhua.commons.utils.JwtUtils;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.vo.UserInfoVo;
import com.tanhua.server.interceptor.UserHolder;
import com.tanhua.server.service.UserInfoService;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/users")
public class UsersController {
    @Autowired
    private UserInfoService userInfoService;

    /**
     * 查询用户资料
     * 1、请求头：token
     * 2、请求参数：userID
     */
    @GetMapping
    public ResponseEntity users(@RequestHeader("Authorization") String token,Long userID) throws Exception{
        //判断用户id
        if(userID==null){
            userID = UserHolder.getUserId();
        }
        UserInfoVo userInfovo = userInfoService.findById(userID);
        return ResponseEntity.ok(userInfovo);
    }

    /**
     * 更新用户资料
     */
    @PutMapping
    public ResponseEntity updateUserInfo(@RequestBody UserInfo userInfo,
                                         @RequestHeader("Authorization") String token){
        //设置用户id
        userInfo.setId(UserHolder.getUserId());
        userInfoService.update(userInfo);
        return ResponseEntity.ok(null);
    }

    /**
     * 更新头像
     */
    @PostMapping("/header")
    public ResponseEntity updateHeader(MultipartFile multipartFile,
                                       @RequestHeader("Authorization") String token) throws IOException {
        userInfoService.updateHead(multipartFile,UserHolder.getUserId());
        return ResponseEntity.ok(null);
    }
}
