package com.tanhua.server.controller;

import com.tanhua.model.mongo.Movement;
import com.tanhua.model.vo.MovementsVo;
import com.tanhua.model.vo.PageResult;
import com.tanhua.model.vo.VisitorsVo;
import com.tanhua.server.service.CommentSerice;
import com.tanhua.server.service.MomentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/movements")
public class MomentController {
    @Autowired
    private MomentService momentService;
    @Autowired
    private CommentSerice commentSerice;

    //发布动态
    @PostMapping
    public ResponseEntity movements(Movement movement, MultipartFile multipartFile[]) throws IOException {
        momentService.publishMoment(movement,multipartFile);
        return ResponseEntity.ok(null);
    }

    //查询我的动态
    @GetMapping("/all")
    public ResponseEntity findByUserId(Long userId,
                                       @RequestParam(defaultValue = "1") Integer page,
                                       @RequestParam(defaultValue = "10") Integer pageSize) {
        PageResult pr = momentService.findByUserId(userId,page,pageSize);
        return ResponseEntity.ok(pr);
    }

    /**
     * 查询好友动态
     */
    @GetMapping
    public ResponseEntity movements(@RequestParam(defaultValue = "1") Integer page,
                                    @RequestParam(defaultValue = "10") Integer pageSize){
        PageResult pr = momentService.findFriendMovements(page,pageSize);
        return ResponseEntity.ok(pr);
    }

    /**
     * 查询推荐动态
     */
    @GetMapping("/recommend")
    public ResponseEntity RecommendMoment(@RequestParam(defaultValue = "1") Integer page,
                                          @RequestParam(defaultValue = "10") Integer pageSize){
        PageResult pr = momentService.findRecommendMovements(page,pageSize);
        return ResponseEntity.ok(pr);
    }

    /**
     * 查询单挑动态
     */
    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable("id") String momentId){
        MovementsVo vo = momentService.findById(momentId);
        return ResponseEntity.ok(vo);
    }

    /**
     * 点赞
     */
    @GetMapping("/{id}/like")
    public ResponseEntity like(@PathVariable("id") String momentId){
        Integer likeCount = commentSerice.likeComment(momentId);
        return ResponseEntity.ok(likeCount);
    }

    /**
     * 取消点赞
     */
    @GetMapping("/{id}/dislike")
    public ResponseEntity dislike(@PathVariable("id") String momentId){
        Integer likeCount = commentSerice.dislikeComment(momentId);
        return ResponseEntity.ok(likeCount);
    }

    /**
     * 喜欢
     */
    @GetMapping("/{id}/love")
    public ResponseEntity love(@PathVariable("id") String momentId){
        Integer loveCount = commentSerice.loveComment(momentId);
        return ResponseEntity.ok(loveCount);
    }

    /**
     * 取消喜欢
     */
    @GetMapping("/{id}/unlove")
    public ResponseEntity dislove(@PathVariable("id") String momentId){
        Integer loveCount = commentSerice.disloveComment(momentId);
        return ResponseEntity.ok(loveCount);
    }

    /**
     * 谁看过我
     */
    @GetMapping("visitors")
    public ResponseEntity queryVisitorsList(){
        List<VisitorsVo> list = momentService.queryVisitorsList();
        return ResponseEntity.ok(list);
    }
}
