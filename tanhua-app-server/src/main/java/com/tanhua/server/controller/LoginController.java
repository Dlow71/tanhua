package com.tanhua.server.controller;

import cn.hutool.json.JSONUtil;
import com.tanhua.model.vo.ErrorResult;
import com.tanhua.server.exception.BusinessException;
import com.tanhua.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/user")
public class LoginController {

    @Autowired
    private UserService userService;


    /**
     * 获取登录验证码
     *   请求参数：phone （Map）
     *   响应：void
     * ResponseEntity
     */
    @PostMapping("/login")   //RequestBody可以自动将Json转成map或者对象
    public ResponseEntity login(@RequestBody Map map){
        String phone =(String) map.get("phone");
        userService.sendMsg(phone);
        //return ResponseEntity.status(500).body("出错啦");
        return ResponseEntity.ok(null); //正常返回状态码200
    }

    /**
     * 检验登录
     */
    @PostMapping("/loginVerification")
    public ResponseEntity loginVerification(@RequestBody Map map) {
//        try{
            //1、调用map集合获取请求参数
            String phone = (String) map.get("phone");
            String code = (String) map.get("verificationCode");
            //2、调用userService完成用户登录
            Map retMap = userService.loginVerification(phone,code);
            //3、构造返回
            System.out.println("------康康检验登录成功后的map集合JSON格式------");
            System.out.println(JSONUtil.toJsonStr(retMap));
            return ResponseEntity.ok(retMap);
//        }catch (BusinessException be){
//            ErrorResult errorResult = be.getErrorResult();
//            //这个常量的值是500
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResult);
//        }catch (Exception e){
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ErrorResult.error());
//        }

    }

}
