package com.tanhua.server.controller;

import com.tanhua.model.vo.PageResult;
import com.tanhua.server.service.CommentSerice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/comments")
public class CommentsController {
    @Autowired
    private CommentSerice commentSerice;

    /**
     * 发布评论
     */
    @PostMapping
    public ResponseEntity saveComments(@RequestBody Map map){
        String movementId = (String) map.get("movementId");
        String comment = (String) map.get("comment");
        commentSerice.saveComments(movementId,comment);
        return ResponseEntity.ok(null);
    }

    /**
     * 分页查询评论列表
     */
    @GetMapping
    public ResponseEntity FindComments(@RequestParam(defaultValue = "1")Integer page,
                                       @RequestParam(defaultValue = "10")Integer pageSize,
                                       String movementId){
        PageResult Pr = commentSerice.findComments(page,pageSize,movementId);
        return ResponseEntity.ok(Pr);
    }
}
