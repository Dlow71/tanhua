package com.tanhua.server.controller;

import com.tanhua.model.vo.PageResult;
import com.tanhua.server.service.SmallVideosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/smallVideos")
public class SmallVideoController {

    @Autowired
    private SmallVideosService videosService;

    @PostMapping
    public ResponseEntity saveVideos(MultipartFile videoTanhuaNail,MultipartFile videoFile) throws Exception{
        videosService.saveVideos(videoTanhuaNail,videoFile);
        return ResponseEntity.ok(null);
    }

    /**
     * 视频列表
     */
    @GetMapping
    public ResponseEntity queryVideoList(@RequestParam(defaultValue = "1")  Integer page,
                                         @RequestParam(defaultValue = "10") Integer pagesize) {
        if(page<1) page = 1;
        PageResult result = videosService.queryVideoList(page, pagesize);
        return ResponseEntity.ok(result);
    }

    /**
     * 关注
     */
    @PostMapping("/{uid}/userFocus")
    public ResponseEntity userFocus(@PathVariable("uid")Long uid){
        videosService.focusUser(uid);
        return ResponseEntity.ok(null);
    }

    /**
     * 取消关注
     */
    @PostMapping("/{uid}/userUnFocus")
    public ResponseEntity userUnFocus(@PathVariable("uid")Long uid){
        videosService.UnfocusUser(uid);
        return ResponseEntity.ok(null);
    }
}