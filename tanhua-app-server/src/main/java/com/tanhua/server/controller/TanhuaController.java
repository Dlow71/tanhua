package com.tanhua.server.controller;

import com.tanhua.model.dto.RecommendUserDto;
import com.tanhua.model.vo.NearUserVo;
import com.tanhua.model.vo.PageResult;
import com.tanhua.model.vo.TodayBest;
import com.tanhua.server.service.TanhuaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/tanhua")
public class TanhuaController {
    @Autowired
    private TanhuaService tanhuaService;

    //今日佳人
    @GetMapping("/todayBest")
    public ResponseEntity todayBest(){
        TodayBest todayBest = tanhuaService.todayBest();
        return ResponseEntity.ok(todayBest);
    }

    //查询分页推荐好友列表
    @GetMapping("/recommendation")
    public ResponseEntity recommendation(RecommendUserDto dto) {
        PageResult pr = tanhuaService.recommendation(dto);
        return ResponseEntity.ok(pr);
    }

    //查看佳人详情
    @GetMapping("/{id}/personalInfo")
    public ResponseEntity recommendDation(@PathVariable("id") Long UserId){
        TodayBest todayBest = tanhuaService.personalInfo(UserId);
        return ResponseEntity.ok(todayBest);
    }

    //查看陌生人问题
    @GetMapping("/strangerQuestions")
    public ResponseEntity questions(Long userId){
        String questions = tanhuaService.stangerQuestions(userId);
        return ResponseEntity.ok(questions);
    }

    //回复陌生人问题
    @PostMapping("/strangerQuestions")
    public ResponseEntity replyQuestions(@RequestBody Map map){
        //前端传递的UserID是Integer类型的，所以要先转成字符串类型，然后再转Long型
        String obj = map.get("userId").toString();
        Long userId = Long.valueOf(obj);
        String reply = map.get("reply").toString();
        tanhuaService.replyQuestions(userId,reply);
        return ResponseEntity.ok(null);
    }

    //推荐用户列表
    @GetMapping("/cards")
    public ResponseEntity queryCardsList(){
        List<TodayBest> list = tanhuaService.queryCardsList();
        return ResponseEntity.ok(list);
    }

    //喜欢
    @GetMapping("{id}/love")
    public ResponseEntity likeUser(@PathVariable(value = "id") Long likeUserId){
        tanhuaService.likeUser(likeUserId);
        return ResponseEntity.ok(null);
    }

    /**
     * 不喜欢
     */
    @GetMapping("{id}/unlove")
    public ResponseEntity<Void> notLikeUser(@PathVariable("id") Long likeUserId) {
        this.tanhuaService.notLikeUser(likeUserId);
        return ResponseEntity.ok(null);
    }

    /**
     * 搜附近
     */
    @GetMapping("/search")
    public ResponseEntity<List<NearUserVo>> queryNearUser(String gender,
                                                          @RequestParam(defaultValue = "2000") String distance) {
        List<NearUserVo> list = this.tanhuaService.queryNearUser(gender, distance);
        return ResponseEntity.ok(list);
    }
}
