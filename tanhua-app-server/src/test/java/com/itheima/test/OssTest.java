package com.itheima.test;

import com.tanhua.autoconfig.template.OssTemplate;
import com.tanhua.server.AppServerApplication;
import org.junit.jupiter.api.Test;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppServerApplication.class)
public class OssTest {
    @Autowired
    private OssTemplate ossTemplate;

    @Test
    public void testTemplate() throws FileNotFoundException {
        //配置图片的路径
        String path = "C:\\Users\\Nu'yo'a'h\\Pictures\\Saved Pictures\\wallhaven-6k97k7 (4).jpg";
        //构造fileInputStream
        FileInputStream fileInputStream = new FileInputStream(new File(path));
        String url = ossTemplate.upload(path, fileInputStream);
        System.out.println(url);
    }

    //案例：上传图片到阿里云oss   存放的位置是/yyyy/MM/dd/xxxx.jpg
    @Test
    public void testOss() throws FileNotFoundException {
        //配置图片的路径
        String path = "C:\\Users\\Nu'yo'a'h\\Pictures\\Saved Pictures\\wallhaven-6k97k7 (4).jpg";
        //构造fileInputStream
        FileInputStream fileInputStream = new FileInputStream(new File(path));
        //拼写图片路径
        String fileName = new SimpleDateFormat("yyyy/MMM/dd").format(new Date())
                + "/" + UUID.randomUUID().toString() + path.substring(path.lastIndexOf("."));


        // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。
        String endpoint = "oss-cn-hangzhou.aliyuncs.com";
        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
        String accessKeyId = "LTAI5tPGbtuFgnaKLQHTnz8X";
        String accessKeySecret = "Qbcvi5HQ5yWhRFGs8qYd0HkhNazi6W";
        // 填写Bucket名称，例如examplebucket。
        String bucketName = "examplebucket";
        // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
        String objectName = "exampledir/exampleobject.txt";

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        try {
            // 填写Byte数组。
            byte[] content = "Hello OSS".getBytes();
            // 创建PutObject请求。
            ossClient.putObject("dlowtanhua001", fileName, fileInputStream);
        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message:" + oe.getErrorMessage());
            System.out.println("Error Code:" + oe.getErrorCode());
            System.out.println("Request ID:" + oe.getRequestId());
            System.out.println("Host ID:" + oe.getHostId());
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message:" + ce.getMessage());
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
                String url = "https://dlowtanhua001.oss-cn-hangzhou.aliyuncs.com/"+ fileName;
                System.out.println(url);
            }
        }
    }
}
