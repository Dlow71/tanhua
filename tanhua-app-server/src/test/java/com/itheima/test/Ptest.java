package com.itheima.test;

import com.rabbitmq.client.impl.Environment;
import com.tanhua.autoconfig.properties.SmsProperties;
import com.tanhua.server.AppServerApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppServerApplication.class)
public class Ptest {
   @Autowired
   private SmsProperties smsProperties;

    @Test
    public void test(){
        System.out.println(smsProperties.getAccessKey());
    }
}
