package com.itheima.test;

import com.baidu.aip.face.AipFace;
import com.tanhua.autoconfig.template.AipFaceTemplate;
import com.tanhua.server.AppServerApplication;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppServerApplication.class)
public class FaceTest {
    @Autowired
    private AipFaceTemplate aipFaceTemplate;

    //设置APPID/AK/SK
    public static final String APP_ID = "26764892";
    public static final String API_KEY = "U5aLFhiwayAijdpSy5voekbu";
    public static final String SECRET_KEY = "QP2DIMH5QZHLLiDWmuH1PN2z0kToj4gV";

    @Test
    public void detect(){
        boolean detect = aipFaceTemplate.detect("https://dlowtanhua001.oss-cn-hangzhou.aliyuncs.com/2022/%E4%B8%83%E6%9C%88/21/91b6d70a-4323-4e6d-a922-a8a5606d5c6f.jpg");
        System.out.println(detect);
    }



    @Test
    public void test(){

        // 初始化一个AipFace
        AipFace client = new AipFace(APP_ID, API_KEY, SECRET_KEY);

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);

        // 可选：设置代理服务器地址, http和socket二选一，或者均不设置
//        client.setHttpProxy("proxy_host", proxy_port);  // 设置http代理
//        client.setSocketProxy("proxy_host", proxy_port);  // 设置socket代理

        // 调用接口
        String image = "https://dlowtanhua001.oss-cn-hangzhou.aliyuncs.com/2022/%E4%B8%83%E6%9C%88/21/91b6d70a-4323-4e6d-a922-a8a5606d5c6f.jpg";
        String imageType = "URL";

        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("face_field", "age");
        options.put("max_face_num", "2");
        options.put("face_type", "LIVE");
        options.put("liveness_control", "LOW");

        // 人脸检测
        JSONObject res = client.detect(image, imageType, options);
        System.out.println(res.toString(2));

    }
}
