package com.itheima.test;

import cn.hutool.core.collection.CollUtil;
import com.easemob.im.server.EMProperties;
import com.easemob.im.server.EMService;
import com.easemob.im.server.model.EMTextMessage;
import com.tanhua.autoconfig.template.HuanXinTemplate;
import com.tanhua.commons.utils.Constants;
import com.tanhua.dubbo.api.UserApi;
import com.tanhua.model.domain.User;
import com.tanhua.server.AppServerApplication;
import org.apache.dubbo.config.annotation.DubboReference;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppServerApplication.class)
public class HuanXinTest {
    private EMService emService;

    @Autowired
    private HuanXinTemplate huanXinTemplate;

    @DubboReference
    private UserApi userApi;

    @Before
    public void init(){
        EMProperties properties = EMProperties.builder()
                .setAppkey("1111220731125048#demo")
                .setClientId("YXA6nNBe96qoTLC6ftQjpEKX7Q")
                .setClientSecret("YXA6k0lpmfIy4EVilekJCq8AWHbXG7Y")
                .build();
        emService = new EMService(properties);
    }

    @Test
    public void test(){
        //注册用户
//        emService.user().create("user02","123456").block();
        //添加联系人
//        emService.contact().add("user01","user02").block();
        //发送消息
        HashSet<String> hashSet = CollUtil.newHashSet("hx36");
        emService.message().send("admin","users",hashSet,
                new EMTextMessage().text("莫西莫西"),null).block();
    }

    @Test
    public void testTemplate(){
        //创建用户
        huanXinTemplate.createUser("user03","123456");
    }

    @Test
    public void register() {
//        for (int i = 1; i < 106; i++) {
            User user = userApi.findById(Long.valueOf(106));
            if(user != null) {
                Boolean create = huanXinTemplate.createUser("hx" + user.getId(), Constants.INIT_PASSWORD);
                if (create){
                    user.setHxUser("hx" + user.getId());
                    user.setHxPassword(Constants.INIT_PASSWORD);
                    userApi.update(user);
                }
            }
//        }
    }
}
