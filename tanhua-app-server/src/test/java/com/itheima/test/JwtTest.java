package com.itheima.test;

import io.jsonwebtoken.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtTest {

    @Autowired
    private Environment environment;

    @Test
    public void env(){
        System.out.println(environment.getProperty("tanhua.sms.secret"));
    }

    @Test
    public void testJWT(){
        //生成token练习
        //1、准备数据
        Map map = new HashMap<>();
        map.put("id",1);
        map.put("phone","13333885566");

        long now = System.currentTimeMillis();
        //2、使用jwt工具生成token
        String token = Jwts.builder()
                .signWith(SignatureAlgorithm.HS256,"itcast") //为了让token更安全，可以再指定一个密钥
                .setClaims(map)
                .setExpiration(new Date(now+30000)) //失效时间
                .compact();
        System.out.println(token);
    }

    //解析token
    @Test
    public void parseToken(){
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJwaG9uZSI6IjEzMzMzODg1NTY2IiwiaWQiOjEsImV4cCI6MTY1NjI1MzM5OH0.WD4bN8CseDzLF70CkxFQQ3-mggr0U0AYUQM1FM7r25U";
        Claims claims = Jwts.parser()
                .setSigningKey("itcast")
                .parseClaimsJws(token)
                .getBody();
        Object id = claims.get("id");
        Object phone = claims.get("phone");
        System.out.println(id+"...."+phone);
    }


    @Test
    public void testCreateToken() {
        //生成token
        //1、准备数据
        Map map = new HashMap();
        map.put("id",1);
        map.put("mobile","13800138000");
        //2、使用JWT的工具类生成token
        long now = System.currentTimeMillis();
        String token = Jwts.builder()
                .signWith(SignatureAlgorithm.HS512, "itcast") //指定加密算法
                .setClaims(map) //写入数据
                .setExpiration(new Date(now + 30000)) //失效时间
                .compact();
        System.out.println(token);
    }

    //解析token

    /**
     * SignatureException : token不合法
     * ExpiredJwtException：token已过期
     */
    @Test
    public void testParseToken() {
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJtb2JpbGUiOiIxMzgwMDEzODAwMCIsImlkIjoxLCJleHAiOjE2MTgzOTcxOTV9.2lQiovogL5tJa0px4NC-DW7zwHFqZuwhnL0HPAZunieGphqnMPduMZ5TtH_mxDrgfiskyAP63d8wzfwAj-MIVw";
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey("itcast")
                    .parseClaimsJws(token)
                    .getBody();
            Object id = claims.get("id");
            Object mobile = claims.get("mobile");
            System.out.println(id + "--" + mobile);
        }catch (ExpiredJwtException e) {
            System.out.println("token已过期");
        }catch (SignatureException e) {
            System.out.println("token不合法");
        }

    }
}
