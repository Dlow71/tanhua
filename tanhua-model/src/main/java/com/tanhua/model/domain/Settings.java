package com.tanhua.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class Settings extends BasePojo {

    private Long id;
    private Long userId;
    private Boolean likeNotification;
    private Boolean pinglunNotification;
    private Boolean gonggaoNotification;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Boolean getLikeNotification() {
        return likeNotification;
    }

    public void setLikeNotification(Boolean likeNotification) {
        this.likeNotification = likeNotification;
    }

    public Boolean getPinglunNotification() {
        return pinglunNotification;
    }

    public void setPinglunNotification(Boolean pinglunNotification) {
        this.pinglunNotification = pinglunNotification;
    }

    public Boolean getGonggaoNotification() {
        return gonggaoNotification;
    }

    public void setGonggaoNotification(Boolean gonggaoNotification) {
        this.gonggaoNotification = gonggaoNotification;
    }

    public Settings() {
    }

    public Settings(Long id, Long userId, Boolean likeNotification, Boolean pinglunNotification, Boolean gonggaoNotification) {
        this.id = id;
        this.userId = userId;
        this.likeNotification = likeNotification;
        this.pinglunNotification = pinglunNotification;
        this.gonggaoNotification = gonggaoNotification;
    }
}