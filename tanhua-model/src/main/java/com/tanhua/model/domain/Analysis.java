package com.tanhua.model.domain;

import java.util.Date;

public class Analysis{

    private Long id;
    /**
     * 日期
     */
    private Date recordDate;
    /**
     * 新注册用户数
     */
    private Integer numRegistered = 0;
    /**
     * 活跃用户数
     */
    private Integer numActive = 0;
    /**
     * 登陆次数
     */
    private Integer numLogin = 0;
    /**
     * 次日留存用户数
     */
    private Integer numRetention1d = 0;

    private Date created;

    public Analysis() {
    }

    public Analysis(Long id, Date recordDate, Integer numRegistered, Integer numActive, Integer numLogin, Integer numRetention1d, Date created) {
        this.id = id;
        this.recordDate = recordDate;
        this.numRegistered = numRegistered;
        this.numActive = numActive;
        this.numLogin = numLogin;
        this.numRetention1d = numRetention1d;
        this.created = created;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    public Integer getNumRegistered() {
        return numRegistered;
    }

    public void setNumRegistered(Integer numRegistered) {
        this.numRegistered = numRegistered;
    }

    public Integer getNumActive() {
        return numActive;
    }

    public void setNumActive(Integer numActive) {
        this.numActive = numActive;
    }

    public Integer getNumLogin() {
        return numLogin;
    }

    public void setNumLogin(Integer numLogin) {
        this.numLogin = numLogin;
    }

    public Integer getNumRetention1d() {
        return numRetention1d;
    }

    public void setNumRetention1d(Integer numRetention1d) {
        this.numRetention1d = numRetention1d;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}