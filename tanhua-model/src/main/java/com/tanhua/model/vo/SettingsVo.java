package com.tanhua.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SettingsVo implements Serializable {

    private Long id;
    private String strangerQuestion = "";
    private String phone;
    private Boolean likeNotification = true;
    private Boolean pinglunNotification = true;
    private Boolean gonggaoNotification = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStrangerQuestion() {
        return strangerQuestion;
    }

    public void setStrangerQuestion(String strangerQuestion) {
        this.strangerQuestion = strangerQuestion;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getLikeNotification() {
        return likeNotification;
    }

    public void setLikeNotification(Boolean likeNotification) {
        this.likeNotification = likeNotification;
    }

    public Boolean getPinglunNotification() {
        return pinglunNotification;
    }

    public void setPinglunNotification(Boolean pinglunNotification) {
        this.pinglunNotification = pinglunNotification;
    }

    public Boolean getGonggaoNotification() {
        return gonggaoNotification;
    }

    public void setGonggaoNotification(Boolean gonggaoNotification) {
        this.gonggaoNotification = gonggaoNotification;
    }
}