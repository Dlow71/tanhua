package com.tanhua.model.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class ErrorResult {

    private String errCode = "999999";
    private String errMessage;

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }

    public ErrorResult() {
    }

    public ErrorResult(String errCode, String errMessage) {
        this.errCode = errCode;
        this.errMessage = errMessage;
    }

    public static ErrorResult error() {
        ErrorResult errorResult = new ErrorResult("999999","系统异常稍后再试");
        return errorResult;
//        return ErrorResult.builder().errCode("999999").errMessage("系统异常稍后再试").build();
    }

    public static ErrorResult fail() {
        ErrorResult errorResult = new ErrorResult("000001","发送验证码失败");
        return errorResult;
//        return ErrorResult.builder().errCode("000001").errMessage("发送验证码失败").build();
    }

    public static ErrorResult loginError() {
        ErrorResult errorResult = new ErrorResult("000002","验证码失效");
        return errorResult;
//        return ErrorResult.builder().errCode("000002").errMessage("验证码失效").build();
    }

    public static ErrorResult faceError() {
        ErrorResult errorResult = new ErrorResult("000003","图片非人像，请重新上传!");
        return errorResult;
//        return ErrorResult.builder().errCode("000003").errMessage("图片非人像，请重新上传!").build();
    }

    public static ErrorResult mobileError() {
        ErrorResult errorResult = new ErrorResult("000004","手机号码已注册");
        return errorResult;
//        return ErrorResult.builder().errCode("000004").errMessage("手机号码已注册").build();
    }

    public static ErrorResult contentError() {
        ErrorResult errorResult = new ErrorResult("000005","动态内容为空");
        return errorResult;
//        return ErrorResult.builder().errCode("000005").errMessage("动态内容为空").build();
    }

    public static ErrorResult likeError() {
        ErrorResult errorResult = new ErrorResult("000006","用户已点赞");
        return errorResult;
//        return ErrorResult.builder().errCode("000006").errMessage("用户已点赞").build();
    }

    public static ErrorResult disLikeError() {
        ErrorResult errorResult = new ErrorResult("000007","用户未点赞");
        return errorResult;
//        return ErrorResult.builder().errCode("000007").errMessage("用户未点赞").build();
    }

    public static ErrorResult loveError() {
        ErrorResult errorResult = new ErrorResult("000008","用户已喜欢");
        return errorResult;
//        return ErrorResult.builder().errCode("000008").errMessage("用户已喜欢").build();
    }

    public static ErrorResult disloveError() {
        ErrorResult errorResult = new ErrorResult("000009","用户未喜欢");
        return errorResult;
//        return ErrorResult.builder().errCode("000009").errMessage("用户未喜欢").build();
    }
}