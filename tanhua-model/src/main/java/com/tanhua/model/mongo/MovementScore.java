package com.tanhua.model.mongo;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("recomment_movement_score")
public class MovementScore {

    private ObjectId id;
    private Long userId;// 用户id
    private Long movementId; //动态id，需要转化为Long类型
    private Double score; //得分
    private Long date; //时间戳

    public MovementScore() {
    }

    public MovementScore(ObjectId id, Long userId, Long movementId, Double score, Long date) {
        this.id = id;
        this.userId = userId;
        this.movementId = movementId;
        this.score = score;
        this.date = date;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getMovementId() {
        return movementId;
    }

    public void setMovementId(Long movementId) {
        this.movementId = movementId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
}