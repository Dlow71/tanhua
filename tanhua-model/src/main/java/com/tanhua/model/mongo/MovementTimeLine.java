package com.tanhua.model.mongo;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

//好友时间线表，用于存储好友发布（或推荐）的数据，每一个用户一张表进行存储
@Document(collection = "movement_timeline")
public class MovementTimeLine implements java.io.Serializable {

    private static final long serialVersionUID = 9096178416317502524L;
    private ObjectId id;
    private ObjectId movementId;//动态id
    private Long userId;   //发布动态用户id
    private Long friendId; // 可见好友id
    private Long created; //发布的时间

    public MovementTimeLine() {
    }

    public MovementTimeLine(ObjectId id, ObjectId movementId, Long userId, Long friendId, Long created) {
        this.id = id;
        this.movementId = movementId;
        this.userId = userId;
        this.friendId = friendId;
        this.created = created;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public ObjectId getMovementId() {
        return movementId;
    }

    public void setMovementId(ObjectId movementId) {
        this.movementId = movementId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFriendId() {
        return friendId;
    }

    public void setFriendId(Long friendId) {
        this.friendId = friendId;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }
}