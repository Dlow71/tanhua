package com.tanhua.admin.Task;

import com.tanhua.admin.service.AnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class AnalysisTask {
    @Autowired
    private AnalysisService analysisService;

    //秒分时日月周年
    @Scheduled(cron = "0/10 * * * * ?")
    public void analysis(){
        //业务逻辑
        try {
            analysisService.analysis();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
