package com.tanhua.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tanhua.model.domain.Analysis;
import com.tanhua.model.domain.Log;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AnalysisMappper extends BaseMapper<Analysis> {
}
