package com.tanhua.admin.service;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tanhua.commons.utils.Constants;
import com.tanhua.dubbo.api.MomentApi;
import com.tanhua.dubbo.api.UserInfoApi;
import com.tanhua.dubbo.api.VideoApi;
import com.tanhua.model.domain.UserInfo;
import com.tanhua.model.mongo.Movement;
import com.tanhua.model.vo.MovementsVo;
import com.tanhua.model.vo.PageResult;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class ManagerService {
    @DubboReference
    private UserInfoApi userInfoApi;
    @DubboReference
    private VideoApi videoApi;
    @DubboReference
    private MomentApi momentApi;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    //用户列表
    public PageResult findAllUsers(Integer page, Integer pagesize) {
        IPage<UserInfo> iPage = userInfoApi.findAll(page, pagesize);
        List<UserInfo> list = iPage.getRecords();
        for (UserInfo userInfo : list) {
            String key = Constants.USER_FREEZE + userInfo.getId();
            if(redisTemplate.hasKey(key)){
                userInfo.setUserStatus("2");
            }
        }
        PageResult pageResult = new PageResult();
        pageResult.setPage(page);
        pageResult.setPages(pagesize);
        pageResult.setCounts((int) iPage.getTotal());
        pageResult.setItems(list);
        return pageResult;
    }

    //查询用户详情
    public ResponseEntity findById(Long userId) {
        UserInfo userInfo = userInfoApi.findById(userId);
        //查询redis中的冻结状态
        String key = Constants.USER_FREEZE + userId;
        if(redisTemplate.hasKey(key)){
            userInfo.setUserStatus("2");
        }
        return ResponseEntity.ok(userInfo);
    }

    //查询指定用户发布的所有视频列表
    public PageResult findAllVideos(Integer page, Integer pagesize, Long uid) {
        return videoApi.findByVids(page,pagesize,uid);
    }

    //查询动态
    public PageResult findAllMovements(Integer page, Integer pagesize, Long uid, Integer state) {
        //调用api，查询数据:moment对象
        PageResult pageResult = momentApi.findByUserId(uid,state,page,pagesize);
        //解析pageResult对象，获取moment列表
        List<Movement> list = (List<Movement>)pageResult.getItems();
        //转化vo
        if(CollUtil.isEmpty(list)){
            return new PageResult();
        }
        List<Long> userIds = CollUtil.getFieldValues(list, "userId", Long.class);
        Map<Long, UserInfo> infoMap = userInfoApi.findByIds(userIds, null);
        List<MovementsVo> vos = new ArrayList<>();
        for (Movement movement : list) {
            UserInfo userInfo = infoMap.get(movement.getUserId());
            if(userInfo!=null){
                MovementsVo vo = MovementsVo.init(userInfo, movement);
                vos.add(vo);
            }
        }
        //构造返回值
        pageResult.setItems(vos);
        return pageResult;
    }

    //用户冻结
    public Map userFreeze(Map params) {
        //构造key
        String userId = params.get("userId").toString();
        String key = Constants.USER_FREEZE + userId;
        //构造失效时间
        Integer freezingTime = Integer.valueOf(params.get("freezingTime").toString());
        int days = 0;
        if(freezingTime == 1){
            days = 3;
        }else if (freezingTime ==2){
            days = 7;
        }
        //将数据存入redis
        String value = JSON.toJSONString(params);
        if(days>0){
            redisTemplate.opsForValue().set(key,value,days, TimeUnit.MINUTES);
        }else {
            redisTemplate.opsForValue().set(key,value);
        }
        Map retMap = new HashMap<>();
        retMap.put("message","冻结成功");
        return retMap;

    }

    //用户解冻
    public Map userUnfreeze(Map params) {
        String userId = params.get("userId").toString();
        String key = Constants.USER_FREEZE + userId;
        redisTemplate.delete(key);
        Map retMap = new HashMap<>();
        retMap.put("message","解冻成功");
        return retMap;
    }
}
