package com.tanhua.admin.service;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tanhua.admin.mapper.AnalysisMappper;
import com.tanhua.admin.mapper.LogMappper;
import com.tanhua.model.domain.Analysis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class AnalysisService {
    @Autowired
    private LogMappper logMappper;

    @Autowired
    private AnalysisMappper analysisMappper;
    /**
     * 定时统计tb_log表中的数据,保存或者更新tb_analysis表
     * 1、查询tb_log表(今日注册数，登录用户数，活跃用户数，次日留存)
     * 2、构造Analysis对象
     * 3、保存或者更新
     */
    public void analysis() throws ParseException {
        //定义查询日期
        String todayStr = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String yesterdayStr = DateUtil.yesterday().toString("yyyy-MM-dd");
        Date todatDate = new SimpleDateFormat("yyyy-MM-dd").parse(todayStr);
        //统计注册数量
        Integer regCount = logMappper.queryByTypeAndLogTime("0102", todayStr);
        //统计登录数量
        Integer loginCount = logMappper.queryByTypeAndLogTime("0101", todayStr);
        //统计活跃数量
        Integer activeCount = logMappper.queryByLogTime(todayStr);
        //统计次日留存
        Integer numRetention = logMappper.queryNumRetention1d(todayStr, yesterdayStr);
        //构造对象

        //根据日期查询数据库
        LambdaQueryWrapper<Analysis> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Analysis::getRecordDate,new SimpleDateFormat("yyyy-MM-dd").parse(todayStr));
        Analysis analysis = analysisMappper.selectOne(wrapper);
        //如果存在就更新，不存在就保存
        if(analysis!=null){
            //8、如果存在，更新
            analysis.setNumRegistered(regCount);
            analysis.setNumLogin(loginCount);
            analysis.setNumActive(activeCount);
            analysis.setNumRetention1d(numRetention);
            analysisMappper.updateById(analysis);
        }else {
            //7、如果不存在，保存
            analysis = new Analysis();
            analysis.setRecordDate(todatDate);
            analysis.setNumRegistered(regCount);
            analysis.setNumLogin(loginCount);
            analysis.setNumActive(activeCount);
            analysis.setNumRetention1d(numRetention);
            analysis.setCreated(new Date());
            analysisMappper.insert(analysis);
        }
    }
}
